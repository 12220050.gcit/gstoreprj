
function deleteItem(row) {
    if (confirm("Are you sure you want to delete this item?")) {
      row.parentNode.removeChild(row);
    }
  }
  function addItem() {
    // Get input field values
    var itemName = document.getElementById("itemName").value;
    var specification = document.getElementById("Specification").value;
    var qty = document.getElementById("Qty").value;
    var price = document.getElementById("Price").value;
  
    // Validate input fields
    if (itemName.trim() === "" || specification.trim() === "" || qty.trim() === "" || price.trim() === "") {
      alert("Please fill in all fields");
      return;
    }
  
    // Create a new row
    var newRow = document.createElement("tr");
  
    // Set the HTML content of the row
    newRow.innerHTML = `
      <td>${itemName}</td>
      <td>${specification}</td>
      <td class="qty">${qty}</td>
      <td class="price">Nu. ${price}</td>
      <td><button class="edit">Edit</button></td>
      <td><button class="delete" onclick="deleteItem(this.parentNode.parentNode)">Delete</button></td>
    `;
  
    // Append the new row to the table
    var table = document.getElementById("items");
    table.appendChild(newRow);
  
    // Clear input fields
    document.getElementById("itemName").value = "";
    document.getElementById("Specification").value = "";
    document.getElementById("Qty").value = "";
    document.getElementById("Price").value = "";
  }
  
  
  
    function editItem(row) {
      var cells = row.getElementsByTagName("td");
      var itemName = cells[0].innerText;
      var specification = cells[1].innerText;
      var qty = cells[2].innerText;
      var price = cells[3].innerText;
      var category = cells[4].innerText;
    
      var imageInput = document.getElementById("imageURL");
    
      // Retrieve the image file if available
      var image = imageInput.files[0];
    
      document.getElementById("itemName").value = itemName;
      document.getElementById("Specification").value = specification;
      document.getElementById("Qty").value = qty;
      document.getElementById("Price").value = price;
      document.getElementById("Category").value = category;
    
      // Display the image preview if available
      if (image) {
        var reader = new FileReader();
        reader.onload = function(e) {
          document.getElementById("imagePreview").src = e.target.result;
        };
        reader.readAsDataURL(image);
      }
    
      // Remove the existing row
      row.remove();
    }
///new code
var reader;

function readURL(input) {
  if (input.files && input.files[0]) {

    reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);

      // createNewDiv(reader.result);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }

}
function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}

$('.image-upload-wrap').bind('dragover', function () {
  $('.image-upload-wrap').addClass('image-dropping');
});

$('.image-upload-wrap').bind('dragleave', function () {
  $('.image-upload-wrap').removeClass('image-dropping');
});



// set the form empty
function resetForm() {
  document.getElementsByTagName("img").value = "";
  document.getElementById("name").value = "";

}

var image =  document.getElementsByTagName("img");
var name = document.getElementsByClassName("card-label")




function insertNewRow(imageUrl) {
   // Get the user input values
     // Get the user input values
     var text = document.getElementById("text").value;
     // Check if the image URL and text input are not empty
     if (!imageUrl || !text) {
      alert("Please upload an image and enter a name before adding a category.");
      return;
   }
  // Check if the image URL and text input are not empty
  else if (!imageUrl) {
    alert("Please upload an image before adding a category.");
    return;
 }

 else if (!text) {
    alert("Please enter a name before adding a category.");
    return;
 }
   // Create a new div element
   var newDiv = document.createElement("div");
 
   // Set some attributes for the new div
   newDiv.className = "new-class";
 
   // Create a new image element
   var image = document.createElement("img");
 
   // Set the source attribute for the image
   image.src = imageUrl;
 
   // Create a new text element
   var textNode = document.createTextNode(text);
 

 
   // Get the table and add a new row
   var table = document.getElementById("category-list");
   var row = table.insertRow(table.rows.length);
 
   // Create new cells in the row
   var cell1 = row.insertCell(0);
   var cell2 = row.insertCell(1);
   var cell3 = row.insertCell(2);
   var cell4 = row.insertCell(3);
 
   // Add the image to the first cell
   cell1.appendChild(image);
 
   // Add the text to the second cell
   cell2.innerHTML = text;
 
   // Add the edit button to the third cell
   cell3.innerHTML = '<input type="button" onclick="updateCategory(this)"value="Edit" id="button-1">';
 
   // Add the delete button to the fourth cell
   cell4.innerHTML = '<input type="button" onclick="deleteCategory(this.parentNode.parentNode)"value="Delete" id="button-2">';

// to remove the image
   removeUpload();

     // Reset the form
  document.getElementById("myForm").reset();
}


function deleteItem(row) {
  if (confirm("Are you sure you want to delete this item?")) {
    row.parentNode.removeChild(row);
  }
}
function addItem() {
  // Get input field values
  var itemName = document.getElementById("itemName").value;
  var specification = document.getElementById("Specification").value;
  var qty = document.getElementById("Qty").value;
  var price = document.getElementById("Price").value;

  // Validate input fields
  if (itemName.trim() === "" || specification.trim() === "" || qty.trim() === "" || price.trim() === "") {
    alert("Please fill in all fields");
    return;
  }

  // Create a new row
  var newRow = document.createElement("tr");

  // Set the HTML content of the row
  newRow.innerHTML = `
    <td>${itemName}</td>
    <td>${specification}</td>
    <td class="qty">${qty}</td>
    <td class="price">Nu. ${price}</td>
    <td><button class="edit">Edit</button></td>
    <td><button class="delete" onclick="deleteItem(this.parentNode.parentNode)">Delete</button></td>
  `;

  // Append the new row to the table
  var table = document.getElementById("items");
  table.appendChild(newRow);

  // Clear input fields
  document.getElementById("itemName").value = "";
  document.getElementById("Specification").value = "";
  document.getElementById("Qty").value = "";
  document.getElementById("Price").value = "";
}



  function editItem(row) {
    var cells = row.getElementsByTagName("td");
    var itemName = cells[0].innerText;
    var specification = cells[1].innerText;
    var qty = cells[2].innerText;
    var price = cells[3].innerText;
    var category = cells[4].innerText;
  
    var imageInput = document.getElementById("imageURL");
  
    // Retrieve the image file if available
    var image = imageInput.files[0];
  
    document.getElementById("itemName").value = itemName;
    document.getElementById("Specification").value = specification;
    document.getElementById("Qty").value = qty;
    document.getElementById("Price").value = price;
    document.getElementById("Category").value = category;
  
    // Display the image preview if available
    if (image) {
      var reader = new FileReader();
      reader.onload = function(e) {
        document.getElementById("imagePreview").src = e.target.result;
      };
      reader.readAsDataURL(image);
    }
  
    // Remove the existing row
    row.remove();
  }
  

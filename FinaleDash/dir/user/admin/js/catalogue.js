window.onload = function(){
  fetch('/categorys')
      .then(response => response.text())
      .then(data => showStudents(data));
      // console.log(data)

}


///new code
var reader;

function readURL(input) {
  if (input.files && input.files[0]) {

    reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);

      // createNewDiv(reader.result);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}

$('.image-upload-wrap').bind('dragover', function () {
  $('.image-upload-wrap').addClass('image-dropping');
});

$('.image-upload-wrap').bind('dragleave', function () {
  $('.image-upload-wrap').removeClass('image-dropping');
});

function InsertNewRow() {
  getFormData()
    .then(data => {
      fetch('/category', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json', // Set the content type to JSON
        },
        body: JSON.stringify(data), // Convert the data object to JSON string
      })
        .then(response1 => {
          if (response1.ok) {
            console.log('Data stored successfully in MongoDB Atlas');
            fetch('/category/' + data.name )
            // Accessing the response body with the help of text() which further returns a promise.
            .then(Response2 => Response2.text())
            .then(data => showStudent(data))
            resetform();
          } else {
            throw new Error('Failed to store data in MongoDB Atlas');
          }
        })
        .catch(error => {
          console.log(error);
        });
    })
    .catch(error => {
      console.error(error);
    });
}


function resetform() {
  removeUpload()
  document.getElementById("text").value = "";
}

function getFormData() {
  const imageUrl = $('.file-upload-image').attr('src');
  console.log(imageUrl);
  return convertImageUrlToByteArray(imageUrl)
    .then(content => {
      const formData = {
        content: imageUrl,
        name: document.getElementById("text").value
      };
      return formData;
    })
    .catch(error => {
      throw new Error(error);
    });
}

function convertImageUrlToByteArray(imageUrl) {
  return fetch(imageUrl)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => new Uint8Array(arrayBuffer))
    .catch(error => {
      throw new Error("Failed to convert image URL to byte array: " + error);
    });
}


function showStudent(data) {
  const student = JSON.parse(data)
  newRow(student)
}

function showStudents(data) {
  const students = JSON.parse(data)
  console.log(students)
  students.forEach(stud => {
      newRow(stud)
  })
}  

function newRow(student) {
  var name = student[1].Value;
  var content = student[2].Value;
  

  // Create a new div element

  var newDiv = document.createElement("div");
  // Set some attributes for the new div
  newDiv.className = "new-class";
  // Create a new link element
  var link = document.createElement("a");
  // Set the href attribute for the link
  link.href = "itemadd.html";

 
  // Create a new image element
  var image = document.createElement("img");
  // Set the source attribute for the image
  image.src = content;
  // Append the image to the link
  link.appendChild(image);
  // Create a new text element
  var textNode = document.createTextNode(name);
  // Get the table and add a new row
  var table = document.getElementById("category-list");
  var row = table.insertRow(table.rows.length);
  // Create new cells in the row
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  // Add the link (with image) to the first cell
  cell1.appendChild(link);
  // Add the text to the second cell
  cell2.appendChild(textNode);
  // Add the edit button to the third cell
  cell3.innerHTML = '<input type="button" onclick="updateStudent(this)" value="Edit" id="button-1">';
  // Add the delete button to the fourth cell
  cell4.innerHTML = '<input type="button" onclick="deleteStudent(this)" value="Delete" id="button-2">';
}


var selectedRow = null
function deleteStudent(r){
  if (confirm("Are you sure you want to DELETE this?")){
      selectedRow = r.parentElement.parentElement;
      sid = selectedRow.cells[1].innerHTML;

      fetch('/category/'+sid,{
          method:"DELETE",
          headers:{"content-type":"application/json; charset=UTF-8"}
      });

      var rowIndex = selectedRow.rowIndex;
      document.getElementById("myTable").deleteRow(rowIndex);
      var rowIndex = selectedRow.rowIndex;
      if(rowIndex>0){
          document.getElementById("myTable").deleteRow(rowIndex);
          selectedRow = null
      }
  }
}

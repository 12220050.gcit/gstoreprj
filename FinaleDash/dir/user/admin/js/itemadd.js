window.onload = function() {
  fetch('/items')
    .then(response => response.text())
    .then(data => showStudents(data));
}

var reader;

function readURL(input) {
  if (input.files && input.files[0]) {
    reader = new FileReader();
    reader.onload = function(e) {
      $('.image-upload-wrap').hide();
      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();
      $('.image-title').html(input.files[0].name);
      // createNewDiv(reader.result);
    };
    reader.readAsDataURL(input.files[0]);
  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}

$('.image-upload-wrap').bind('dragover', function() {
  $('.image-upload-wrap').addClass('image-dropping');
});

$('.image-upload-wrap').bind('dragleave', function() {
  $('.image-upload-wrap').removeClass('image-dropping');
});

function InsertNewRow() {
  getFormData()
    .then(data => {
      fetch('/item', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json', // Set the content type to JSON
        },
        body: JSON.stringify(data), // Convert the data object to JSON string
        })
        // console.log(data)
        .then(response1 => {
          if (response1.ok) {
            console.log('Data stored successfully in MongoDB Atlas');
            fetch('/item/' + data.name + '/' + data.catname)
              .then(Response2 => Response2.text())
              .then(data => showStudent(data));
            resetform();
          } else {
            throw new Error('Failed to store data in MongoDB Atlas');
          }
        })
        .catch(error => {
          console.log(error);
        });
    })
    .catch(error => {
      console.error(error);
    });
}

function resetform() {
  removeUpload();
  document.getElementById("itemName").value = "";
  document.getElementById("Qty").value = "";
  document.getElementById("Specification").value = "";
  document.getElementById("Category").value = "";
}

function getFormData() {
  const imageUrl = $('.file-upload-image').attr('src');
  console.log(imageUrl);
  return convertImageUrlToByteArray(imageUrl)
    .then(content => {
      const formData = {
        catname: document.getElementById("Category").value,
        content: imageUrl,
        name: document.getElementById("itemName").value,
        quantity: parseInt(document.getElementById("Qty").value),
        specification: document.getElementById("Specification").value
      };
      return formData;
    })
    .catch(error => {
      throw new Error(error);
    });
}

function convertImageUrlToByteArray(imageUrl) {
  return fetch(imageUrl)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => new Uint8Array(arrayBuffer))
    .catch(error => {
      throw new Error("Failed to convert image URL to byte array: " + error);
    });
}

function showStudent(data) {
  const student = JSON.parse(data);
  newRow(student);
}

function showStudents(data) {
  const students = JSON.parse(data);
  console.log(students);
  students.forEach(stud => {
    newRow(stud);
  });
}
function newRow(student) {
  console.log(student);

  var name = student.name;
  var content = student.content;
  var quantity = student.quantity;
  var category = student.catname;
  var specification = student.specification;

  console.log(name);
  console.log(content);

  var image = document.createElement("img");
  image.src = content;

  var textNode = document.createTextNode(name);
  var table = document.getElementById("category-list");
  var row = table.insertRow(table.rows.length);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);
  var cell7 = row.insertCell(6);
 



  cell1.textContent = category;
  cell2.appendChild(image);
  cell3.appendChild(textNode);
  cell4.textContent = quantity;
  cell5.textContent = specification;
  cell6.innerHTML = '<input type="button" onclick="updateStudent(this)" value="Edit" id="button-1">';
  cell7.innerHTML += '<input type="button" onclick="deleteStudent(this)" value="Delete" id="button-2">';
}

var selectedRow = null;

function deleteStudent(r) {
  if (confirm("Are you sure you want to delete this?")) {
    selectedRow = r.parentElement.parentElement;
    var itemName = selectedRow.cells[2].textContent;
    var catName = selectedRow.cells[0].textContent;

    fetch('/item/' + itemName + '/' + catName, {
      method: "DELETE",
      headers: { "Content-Type": "application/json; charset=UTF-8" }
    })
    .then(response => {
      if (response.ok) {
        var rowIndex = selectedRow.rowIndex;
        document.getElementById("category-list").deleteRow(rowIndex);
        selectedRow = null;
      } else {
        throw new Error('Failed to delete the item');
      }
    })
    .catch(error => {
      console.log(error);
    });
  }
}
function update(itemName, catName) {
  var formData = getFormData();

  // Create the payload for the update request
  var updatedItem = {
    name: formData.name,
    quantity: formData.quantity,
    content: formData.content,
    specification: formData.specification,
    catname: formData.catname
  };

  // Send the update request to the server
  fetch(`/item/${encodeURIComponent(itemName)}/${encodeURIComponent(catName)}`, {
    method: "PUT",
    headers: { "Content-Type": "application/json; charset=UTF-8" },
    body: JSON.stringify(updatedItem)
  })
    .then(response => {
      if (response.ok) {
        console.log("Item updated successfully");
        // Update the corresponding row in the table with the updated values
        selectedRow.cells[0].innerHTML = formData.catname;
        selectedRow.cells[1].innerHTML = `<img src="${formData.content}" />`;
        selectedRow.cells[2].innerHTML = formData.name;
        selectedRow.cells[3].innerHTML = formData.specification;
        selectedRow.cells[4].innerHTML = formData.quantity;

        var btn = document.getElementById("button-add");
        if (btn) {
          btn.innerHTML = "Add";
          btn.setAttribute("onclick", "InsertNewRow()");
          selectedRow = null;
          resetform();
        } else {
          alert("Server: Update request error.");
        }
      } else {
        throw new Error("Failed to update the item");
      }
    })
    .catch(error => {
      console.log(error);
    });
}

function updateStudent(r) {
  selectedRow = r.parentElement.parentElement;
  // Retrieve the updated values from the form
  document.getElementById("file-upload-image").src = selectedRow.cells[1].querySelector('img').src;
  document.getElementById("itemName").value = selectedRow.cells[2].innerHTML;
  document.getElementById("Qty").value = selectedRow.cells[4].innerHTML;
  document.getElementById("Specification").value = selectedRow.cells[3].innerHTML;
  document.getElementById("Category").value = selectedRow.cells[0].innerHTML;

  var btn = document.getElementById("button-add");
  itemName = selectedRow.cells[2].innerHTML;
  catName = selectedRow.cells[0].innerHTML;

  if (btn) {
    btn.innerHTML = "Update";
    btn.setAttribute("onclick", `update('${itemName}', '${catName}')`);
  }
}

const form = document.querySelector("#create-account-form");
const userIdInput = document.querySelector("#userid");
const emailInput = document.querySelector("#email");
const passwordInput = document.querySelector("#password");

form.addEventListener("submit", (event) => {
  validateForm();
  console.log(isFormValid());
  if (isFormValid() == true) {
    form.submit();
    // window.open("cata.html")
  } else {
    event.preventDefault();
  }
});

function isFormValid() {
  const inputContainers = form.querySelectorAll(".input-group");
  let result = true;
  inputContainers.forEach((container) => {
    if (container.classList.contains("error")) {
      result = false;
    }
  });
  return result;
}

function validateForm() {
  //EMAIL
  if (emailInput.value.trim() == "") {
    setError(emailInput, "Provide email address");
  } else if (isEmailValid(emailInput.value)) {
    setSuccess(emailInput);
  } else {
    setError(emailInput, "Provide valid email address");
  }

  //PASSWORD
  if (passwordInput.value.trim() == "") {
    setError(passwordInput, "Password can not be empty");
  } else if (
    passwordInput.value.trim().length < 6 ||
    passwordInput.value.trim().length > 20
  ) {
    setError(passwordInput, "Password min 6 max 20 charecters");
  } else {
    setSuccess(passwordInput);
  }
}

function setError(element, errorMessage) {
  const parent = element.parentElement;
  if (parent.classList.contains("success")) {
    parent.classList.remove("success");
  }
  parent.classList.add("error");
  const paragraph = parent.querySelector("p");
  paragraph.textContent = errorMessage;
}

function setSuccess(element) {
  const parent = element.parentElement;
  if (parent.classList.contains("error")) {
    parent.classList.remove("error");
  }
  parent.classList.add("success");
}

function isEmailValid(email) {
  const reg =
  /^[A-Za-z0-9]+(?:\w+\.)?gcit@rub\.edu\.bt$/
  return reg.test(email);
}


function Login() {
  var data = {
    id : document.getElementById("userid").value,
    password: document.getElementById("password").value,
  }
  fetch("/login",{
    method:"POST",
    body:JSON.stringify(data),
    headers:{"content-type":"application/json; charset-utf 8"}
  }).then(res =>{
    if (res.status == 202){
      if (data.id=="rub1234567"){
        console.log(202)
        window.location.href = "dash_admin.html"
      }else{
        console.log("two0two")
        window.location.href = "cata.html"
      }
      
    }else if (res.status == 404){
      alert("Invalid login")
    }
    else{
      throw new Error(res)
    }
  })
}
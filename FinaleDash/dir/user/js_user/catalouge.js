window.onload = function(){
  fetch('/categorys')
      .then(response => response.text())
      .then(data => showStudents(data));
      // console.log(data)

}

function showStudents(data) {
  const students = JSON.parse(data)
  console.log(students)
  students.forEach(stud => {
      newRow(stud)
  })
}  


function newRow(student) {
  console.log(student)
  var name = student[1].Value;
  var content = student[2].Value;
  // Select the card-container element
  const cardContainer = document.querySelector('.card-container');
  // Create the card element
  const card = document.createElement('div');
  card.classList.add('card');
  // Create the image element
  const image = document.createElement('img');
  image.src = content;
  image.alt = 'Catgory';
  // Create the link element
  const link = document.createElement('a');
  link.href = 'Furniture.html';
  link.textContent = name;
  // Append the image and link elements to the card element
  card.appendChild(image);
  card.appendChild(link);
  // Append the card element to the card-container element
  cardContainer.appendChild(card);
}



package main

import (
	"finalDash/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	// handle item requests
	router.HandleFunc("/item", controller.ItemHandler).Methods("POST")
	router.HandleFunc("/item/{Itemname}/{Catname}", controller.GetItem).Methods("GET")
	router.HandleFunc("/item/{Itemname}/{Catname}", controller.UpdateItem).Methods("PUT")
	router.HandleFunc("/item/{Itemname}/{Catname}", controller.DeleteItem).Methods("DELETE")
	router.HandleFunc("/items", controller.GetAllItemsHandler).Methods("GET")

	// // handle item requests
	router.HandleFunc("/category", controller.CategoryHandler).Methods("POST")
	router.HandleFunc("/category/{categoryName}", controller.GetCatHandler).Methods("GET")
	router.HandleFunc("/category/{categoryName}", controller.CategoryDelete).Methods("DELETE")
	router.HandleFunc("/categorys", controller.GetAllCollectionsHandler).Methods("GET")

	router.HandleFunc("/user", controller.CreateUser).Methods("POST")
	router.HandleFunc("/login", controller.LoginHandler).Methods("POST")
	fhandler := http.FileServer(http.Dir("./dir"))
	router.PathPrefix("/").Handler(fhandler)
	http.ListenAndServe(":8082", router)
}

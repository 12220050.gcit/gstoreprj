package model

import (
	"context"
	mongosh "finalDash/datastore"
	"fmt"
	"unicode"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Category struct {
	Name    string `json:"name"`
	Content string `json:"content"`
	Items   []Item `json:"items"`
}

// Item struct
type Item struct {
	CatName  string `json:"catname"`
	Name     string `json:"name"`
	Quantity int64  `json:"quantity"`
	Detail   string `json:"detail"`
	Content  string `json:"content"`
}

func (c *Category) Create() {
	collection := mongosh.Client.Database("store").Collection(c.Name)
	_, err := collection.InsertOne(context.Background(), c)
	if err != nil {
		fmt.Println("Failed to create category:", err)
		return
	}

	fmt.Println("Category created successfully.")
}

func (c *Category) DeleteCollection() error {
	collection := mongosh.Client.Database("store").Collection(c.Name)

	err := collection.Drop(context.Background())
	if err != nil {
		fmt.Println("Failed to delete category collection:", err)
		return err
	}

	fmt.Println("Category collection deleted successfully.")
	return nil
}

// Get all collections in the database
// Get all collections in the database

func (i Category) GetAllCollections() ([]string, error) {
	// Get a list of all collections
	allCollections, err := mongosh.Client.Database("store").ListCollectionNames(context.Background(), bson.M{})
	if err != nil {
		fmt.Println("Failed to retrieve collections:", err)
		return nil, err
	}

	// Filter collections with uppercase starting name
	var collections []string
	for _, collection := range allCollections {
		if len(collection) > 0 && unicode.IsUpper(rune(collection[0])) {
			collections = append(collections, collection)
		}
	}

	return collections, nil
}

// Get documents in all collections
func (c Category) GetAllDocumentsInCollections(collections []string) ([]interface{}, error) {
	var documents []interface{}

	for _, collection := range collections {
		collectionDocs, err := c.GetDocumentsInCollection(collection)
		if err != nil {
			fmt.Println("Failed to retrieve documents from collection:", collection)
			return nil, err
		}

		documents = append(documents, collectionDocs...)
	}

	return documents, nil
}

// Get documents in a collection
func (c Category) GetDocumentsInCollection(collectionName string) ([]interface{}, error) {
	collection := mongosh.Client.Database("store").Collection(collectionName)

	// Retrieve all documents in the collection
	cursor, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		fmt.Println("Failed to retrieve documents from collection:", err)
		return nil, err
	}
	defer cursor.Close(context.Background())

	// Iterate over the cursor and collect the documents
	var documents []interface{}
	for cursor.Next(context.Background()) {
		var doc interface{}
		if err := cursor.Decode(&doc); err != nil {
			fmt.Println("Failed to decode document:", err)
			return nil, err
		}
		documents = append(documents, doc)
	}

	if err := cursor.Err(); err != nil {
		fmt.Println("Cursor error:", err)
		return nil, err
	}

	return documents, nil
}

// creating items
func (i *Item) CreateI(categoryName string) {
	collection := mongosh.Client.Database("store").Collection(i.CatName)
	filter := bson.D{{Key: "name", Value: categoryName}}

	var category Category
	err := collection.FindOne(context.Background(), filter).Decode(&category)
	if err != nil {
		fmt.Println("Failed to retrieve category:", err)
		return
	}

	newItem := Item{
		CatName:  i.CatName,
		Content:  i.Content,
		Name:     i.Name,
		Quantity: i.Quantity,
		Detail:   i.Detail,
	}

	category.Items = append(category.Items, newItem)

	update := bson.D{{Key: "$set", Value: bson.D{{Key: "items", Value: category.Items}}}}
	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		fmt.Println("Failed to update category:", err)
		return
	}

	fmt.Println("Item created and added to the category successfully.")
}

func (b *Item) GetCategory(categoryName string) (*Category, error) {
	// Retrieve the category from the database based on the category name
	// Replace with the actual category name

	collection := mongosh.Client.Database("store").Collection(categoryName)

	filter := bson.D{{Key: "name", Value: categoryName}}

	category := &Category{}
	err := collection.FindOne(context.Background(), filter).Decode(&category)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			fmt.Println("Category not found:", err)
			return nil, err
		}

		fmt.Println("Failed to retrieve category:", err)
		return nil, err
	}

	return category, nil
}

func (c *Category) UpdateCategory(category *Category) error {
	// Update the category in the database based on the category name
	// Replace with the actual category name

	collection := mongosh.Client.Database("store").Collection(category.Name)

	filter := bson.D{{Key: "name", Value: category.Name}}
	update := bson.D{{Key: "$set", Value: category}}

	_, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		fmt.Println("Failed to update category:", err)
		return err
	}

	return nil
}

func (c *Category) UpdateCategoryI(category *Category) error {
	// Update the category in the database based on the category name
	// Replace with the actual database update code

	collection := mongosh.Client.Database("store").Collection(category.Name)

	filter := bson.D{{Key: "name", Value: category.Name}}
	update := bson.D{{Key: "$set", Value: bson.D{{Key: "items", Value: category.Items}}}}

	_, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		fmt.Println("Failed to update category:", err)
		return err
	}

	return nil
}

// Get items in all collections
func (c Category) GetItemsInCollection(collectionName string) ([]Item, error) {
	collection := mongosh.Client.Database("store").Collection(collectionName)

	// Retrieve all documents in the collection
	cursor, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		fmt.Println("Failed to retrieve documents from collection:", err)
		return nil, err
	}
	defer cursor.Close(context.Background())

	// Iterate over the cursor and collect the items
	var items []Item
	for cursor.Next(context.Background()) {
		var category Category
		if err := cursor.Decode(&category); err != nil {
			fmt.Println("Failed to decode document:", err)
			return nil, err
		}
		items = append(items, category.Items...)
	}

	if err := cursor.Err(); err != nil {
		fmt.Println("Cursor error:", err)
		return nil, err
	}

	return items, nil
}

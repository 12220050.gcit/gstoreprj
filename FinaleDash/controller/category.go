package controller

import (
	"encoding/json"
	"finalDash/model"
	httpResp "finalDash/response"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// creating category
func CategoryHandler(w http.ResponseWriter, r *http.Request) {
	var chala model.Category

	// Parse the request body
	decoder := json.NewDecoder(r.Body)

	// Storing the data in chala variable
	err := decoder.Decode(&chala)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid JSON data")
		fmt.Println(err)
		return
	}

	chala.Create()

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Item Added"})
}

// creating items
func ItemHandler(w http.ResponseWriter, r *http.Request) {
	var items model.Item

	decoder := json.NewDecoder(r.Body)
	// Storing the data in stud variable
	err := decoder.Decode(&items)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json data")
		fmt.Println(err)
	}
	items.CreateI(items.CatName)
	// fmt.Println(chala)
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Item Added"})
}

func CategoryDelete(w http.ResponseWriter, r *http.Request) {
	categoryName := mux.Vars(r)["categoryName"]

	if r.Method != http.MethodDelete {
		httpResp.RespondWithError(w, http.StatusMethodNotAllowed, "Invalid request method")
		return
	}

	category := model.Category{Name: categoryName}
	err := category.DeleteCollection()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "Category Collection Deleted"})
}

// Retrieve all collections and their documents
func GetAllCollectionsHandler(w http.ResponseWriter, r *http.Request) {
	var category model.Category
	collections, err := category.GetAllCollections()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to retrieve collections")
		fmt.Println(err)
		return
	}

	var documents []interface{}
	for _, collection := range collections {
		category.Name = collection
		collectionDocs, err := category.GetDocumentsInCollection(collection)
		if err != nil {
			httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to retrieve documents from collection: "+collection)
			fmt.Println(err)
			return
		}

		documents = append(documents, collectionDocs...)
	}

	httpResp.RespondWithJSON(w, http.StatusOK, documents)
}

// getting individuals item
func GetCatHandler(w http.ResponseWriter, r *http.Request) {
	// Get the ItemId and CatName parameters from the request URL

	cid := mux.Vars(r)["categoryName"]

	// Create an instance of Item
	item := model.Item{CatName: cid}

	// Retrieve the category from the database
	category, err := item.GetCategory(item.CatName)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, category)

}

// getting individuals item
func GetItem(w http.ResponseWriter, r *http.Request) {
	// Get the ItemId parameter from the request URL
	ssid := mux.Vars(r)["Itemname"]
	cid := mux.Vars(r)["Catname"]

	// Create an instance of Items
	stud := model.Item{Name: ssid, CatName: cid}

	// Retrieve the category from the database
	category, err := stud.GetCategory(stud.CatName)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	// Find the item within the category's items
	foundItem := FindItemByID(category.Items, ssid)
	if foundItem != nil {
		// Respond with the found item
		httpResp.RespondWithJSON(w, http.StatusOK, *foundItem)
	} else {
		// If no item is found, respond with an error
		httpResp.RespondWithError(w, http.StatusNotFound, "Item not found")
	}
}

// helper function for above
func FindItemByID(items []model.Item, itemname string) *model.Item {
	for _, item := range items {
		if item.Name == itemname {
			return &item
		}
	}

	return nil
}

// deleting item
func DeleteItem(w http.ResponseWriter, r *http.Request) {
	// Get the ItemId parameter from the request URL
	ssid := mux.Vars(r)["Itemname"]
	cid := mux.Vars(r)["Catname"]

	// Create an instance of Item
	item := model.Item{Name: ssid, CatName: cid}

	// Retrieve the category from the database
	category, err := item.GetCategory(item.CatName)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	// Find the index of the item within the category's items
	index := FindItemIndexByID(category.Items, ssid)
	if index != -1 {
		// Remove the item from the category's items
		category.Items = append(category.Items[:index], category.Items[index+1:]...)

		// Update the category in the database
		err = category.UpdateCategory(category)
		if err != nil {
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		httpResp.RespondWithJSON(w, http.StatusOK, "Item deleted successfully")
	} else {
		// If no item is found, respond with an error
		httpResp.RespondWithError(w, http.StatusNotFound, "Item not found")
	}
}

// helper function for above
func FindItemIndexByID(items []model.Item, itemname string) int {
	for index, item := range items {
		if item.Name == itemname {
			return index
		}
	}

	return -1
}

// updating item properties
func UpdateItem(w http.ResponseWriter, r *http.Request) {
	// Get the ItemId and CatName parameters from the request URL
	ssid := mux.Vars(r)["Itemname"]
	cid := mux.Vars(r)["Catname"]

	// Create an instance of Item
	item := model.Item{Name: ssid, CatName: cid}

	// Retrieve the category from the database
	category, err := item.GetCategory(item.CatName)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	decoder := json.NewDecoder(r.Body)
	// Storing the data in stud variable
	erro := decoder.Decode(&item)

	if erro != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json data")
		fmt.Println(err)
	}
	// Find the item within the category's items
	itemIndex := FindItemIndexByID(category.Items, ssid)
	if itemIndex != -1 {
		// Update the properties of the item
		category.Items[itemIndex].Name = item.Name
		category.Items[itemIndex].Quantity = item.Quantity
		category.Items[itemIndex].Detail = item.Detail

		// Update the category in the database
		err = category.UpdateCategoryI(category)
		if err != nil {
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		httpResp.RespondWithJSON(w, http.StatusOK, "Item updated successfully")
	} else {
		// If no item is found, respond with an error
		httpResp.RespondWithError(w, http.StatusNotFound, "Item not found")
	}
}

// Retrieve all items from all collections
func GetAllItemsHandler(w http.ResponseWriter, r *http.Request) {
	var category model.Category
	collections, err := category.GetAllCollections()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to retrieve collections")
		fmt.Println(err)
		return
	}

	var items []model.Item
	for _, collection := range collections {
		collectionItems, err := category.GetItemsInCollection(collection)
		if err != nil {
			httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to retrieve items from collection: "+collection)
			fmt.Println(err)
			return
		}

		items = append(items, collectionItems...)
	}

	httpResp.RespondWithJSON(w, http.StatusOK, items)
}

package controller

import (
	"context"
	"encoding/json"
	"finalDash/model"
	httpResp "finalDash/response"
	"fmt"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

// type UserController struct{
// 	Session *mgo.Session
// }

// func NewUserController(s *mgo.Session) *UserController{
// 	return &UserController{s}
// }

// func (uc UserController) GetUser (w http.ResponseWriter, r *http.Request, p httprouter.Params) {
// 	id := p.ByName("id")

// 	if !bson.IsObjectIdHex(id){
// 		w.WriteHeader(http.StatusNotFound)
// 	}
// 	 oid := bson.ObjectHex(id)

// 	u := model.User{}
// 	if err := uc.Session.DB("mongo-golang").C("users").FindId(oid).One(&u); err != nil {
// 		w.WriteHeader(404)
// 		return
// 	}
// 	uj, err := json.Marshal(u)
// 	if err != nil{
// 		fmt.Println(err)
// 	}
// 	w.Header().Set("content-Type", "application/json")
// 	w.WriteHeader(http.StatusOK)
// 	fmt.Fprintf(w, "%s\n", uj)
// }

func CreateUser (w http.ResponseWriter, r *http.Request,) {
	var user model.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		fmt.Print("error in decoding request",err)
		httpResp.RespondWithError(w,http.StatusBadRequest,err.Error())
		return 
	}
	fmt.Println(user.Password)
	var hash []byte
	hash, err = bcrypt.GenerateFromPassword([]byte(user.Password),bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("bcrypt err:",err)
		httpResp.RespondWithError(w,http.StatusNotAcceptable,"error encrypting the password")
	}
	fmt.Println("hash:",hash)
	fmt.Println("string(hash)",string(hash))
	
	addErr := user.AddUser(hash)
	if addErr != nil {
		fmt.Print("error in adding the user",addErr)
		httpResp.RespondWithError(w,http.StatusBadRequest,addErr.Error())
		return 
	}
	httpResp.RespondWithJSON(w,http.StatusCreated,"added successsfully")
	fmt.Println(user)
	fmt.Println("I am empty",user.Password)

	// user.Id = bson.NewObjectId()

	// uc.Session.DB("mongo-golang").C("Users").Insert(u)

	// uj, err := json.Marshal(u)
	// if err != nil{
	// 	fmt.Println(err)
	// }
	// w.Header().Set("content-Type", "application/json")
	// w.WriteHeader(http.StatusOK)
	// fmt.Fprintf(w, "%s\n", uj)
}


func LoginHandler(w http.ResponseWriter,r *http.Request) {
	fmt.Println("hello")
	var user model.User
	var err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w,http.StatusBadRequest,err.Error())
		fmt.Println("error in decoding the request")
		return 
	}
	fmt.Println("empty:",user.Id)
	cur, _ := user.Check()
	// if checkErr != nil {
	// 	switch checkErr {
	// 	case mongo.ErrNoDocuments:
	// 		httpResp.RespondWithError(w,http.StatusNotFound,"invalid login")
	// 		fmt.Println("invalid login from id")
	// 	default:
	// 		fmt.Println("error in checking")
	// 		httpResp.RespondWithError(w,http.StatusBadRequest,"error in checking")
	// 	}
	// 	return 
	// }
	fmt.Println(cur)
	defer cur.Close(context.Background())

    // Decode the first document in the cursor into a User object
    var user1 model.User
    if cur.Next(context.Background()) {
        if err := cur.Decode(&user1); err != nil {
             fmt.Printf("error decoding document: %v", err)
			 httpResp.RespondWithError(w,http.StatusInternalServerError,"error in decoding the document")
			 return 
        }
		
    } else {
        // Handle the case where the cursor is empty
        fmt.Println("error from email")
		httpResp.RespondWithError(w,http.StatusNotFound,"invalid login")
		return 
    }

    // Compare the decoded User object with other data as needed
    // ...
	pwdErr := bcrypt.CompareHashAndPassword([]byte(user1.Password),[]byte(user.Password))
	if pwdErr != nil{
		fmt.Println("error from password")
		httpResp.RespondWithError(w,http.StatusNotFound,"invalid login")
		return 
	}
	fmt.Println("login successful")
	httpResp.RespondWithJSON(w,http.StatusAccepted,map[string]string{"message":"successful"})
}

// func  DeleteUser(w http.ResponseWriter, r *http.Request) {

// 	id := p.ByName("id")
// 	if !bson.IsObjectIdHex(id) {
// 		w.WriteHeader(404)
// 		return
// 	}

// 	oid := bson.ObjectIdHex(id)
// 	if err := uc.Session.DB("mongo-golang").C("users").RemoveId(oid); err !=nil {
// 		w.WriteHeader(404)
// 	}
// 	w.WriteHeader(http.StatusOK)
// 	fmt.Fprint(w, "Deleted user", oid, "\n")

// }